FROM debian:latest
MAINTAINER Alberto Mendoza <mendozaene@gmail.com>
ENV PATH=$PATH:/root/.composer/vendor/bin/
RUN apt-get update \
	&& apt-get -y install apache2 php5 php5-mysql php5-pgsql git curl \ 
	&& a2enmod rewrite \
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
	&& composer global require "laravel/installer" \
	&& composer global require "laravel/lumen-installer"
WORKDIR /etc/apache2/
ADD apache2-conf.tar ./
WORKDIR /var/www/html/
CMD /usr/sbin/apache2ctl -D FOREGROUND